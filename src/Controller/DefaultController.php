<?php

namespace App\Controller;

use App\Services\Interfaces\CalculateInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Класс, реализующий получение строки и вывод результата
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends AbstractController
{

    /**
     * Главная страница. Выводится форма через которую можно ввести строку для расчёта
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('home_page/index.html.twig');
    }

    /**
     * Метод, работающий только через POST(как это указано в config/routes.yaml)
     * На вход получает строку из формы, валидирует её, отправляет на сервис, который
     * считает результат
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param CalculateInterface $calcService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function calculate(Request $request, ValidatorInterface $validator, CalculateInterface $calcService)
    {
        $testStr = trim($request->get('testInput'));

        $errors = $validator->validate($testStr, [
            new NotBlank(["message" => "Выражение не может быть пустым"]),
            new Regex(["pattern" => "/^(\d+\s*(\+|\-|\/|\*)\s*)+\d+\s*$/",
                "message" => " В выражении допускаются только цифры, 
                знаки пробела и знаки арифметических действий + - * /"
            ])
        ]);

        if (0 !== count($errors)) {
            return $this->render('home_page/calculate.html.twig', [
                "errors" => $errors
            ]);
        }

        //Оборачиваем всё в try catch,
        // так как в дальнейшём возможно расширение и обращение
        //к другим системам
        try {
            $calcResult = $calcService->calculate($testStr);

            //Ошибки обработанные и перехваченные на уровне сервиса
            if (!$calcResult) {
                return $this->render('home_page/calculate.html.twig', [
                    "errors" => $calcService->getErrors()
                ]);
            }

        } catch (\Exception $e) {
            $unexpectedErrors = array($e);
            return $this->render('home_page/calculate.html.twig', [
                "errors" => $unexpectedErrors
            ]);
        }

        //Выводим результат
        return $this->render('home_page/calculate.html.twig', [
            "result" => $calcService->getResult()
        ]);
    }
}
