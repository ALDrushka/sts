<?php

namespace App\Services;

use App\Services\Interfaces\CalculateInterface;

class CalculateService implements CalculateInterface
{
    private $result;
    private $errors;

    //Арифметические действия с приорететом
    private $delemeters = ["+" => 0, "-" => 0, "/" => 1, "*" => 1];

    /**
     * Метод получает на вход строку и возвращает true, если выражение удалось посчитать
     * Валидация происходит в контроллере
     *
     * @param string $inputStr
     * @return bool
     */
    public function calculate(string $inputStr): bool
    {
        $calcQueue = array();
        $operStack = array();
        $operPriority = $this->delemeters;
        $token = '';
        foreach (str_split($inputStr) as $char) {
            // Если цифра, то собираем из цифр число
            if ($char >= '0' && $char <= '9') {
                $token .= $char;
            } else {
                // Если число накопилось, сохраняем в очереди вычисления
                if (strlen($token)) {
                    array_push($calcQueue, $token);
                    $token = '';
                }
                // Если найденный символ - операция (он есть в списке приоритетов)
                if (isset($operPriority[$char])) {
                    // Встретили операцию. Переносим операции с меньшим приоритетом в очередь вычисления
                    while (!empty($operStack)) {
                        $oper = array_pop($operStack);
                        if ($operPriority[$char] > $operPriority[$oper]) {
                            array_push($operStack, $oper);
                            break;
                        }
                        array_push($calcQueue, $oper);
                    }
                    // Кладем операцию на стек операций
                    array_push($operStack, $char);
                }
            }

        }
        // Вроде все разобрали, но если остались циферки добавляем их в очередь вычисления
        if (strlen($token)) {
            array_push($calcQueue, $token);
            $token = '';
        }
        // ... и оставшиеся в стеке операции
        if (!empty($operStack)) {
            while ($oper = array_pop($operStack)) {
                array_push($calcQueue, $oper);
            }
        }
        $calcStack = array();
        // Теперь вычисляем все то, что напарсили
        foreach ($calcQueue as $token) {
            switch ($token) {
                case '+':
                    $arg2 = array_pop($calcStack);
                    $arg1 = array_pop($calcStack);
                    array_push($calcStack, $arg1 + $arg2);
                    break;
                case '-':
                    $arg2 = array_pop($calcStack);
                    $arg1 = array_pop($calcStack);
                    array_push($calcStack, $arg1 - $arg2);
                    break;
                case '*':
                    $arg2 = array_pop($calcStack);
                    $arg1 = array_pop($calcStack);
                    array_push($calcStack, $arg1 * $arg2);
                    break;
                case '/':
                    $arg2 = array_pop($calcStack);
                    $arg1 = array_pop($calcStack);
                    if($arg2 == 0){
                        $this->errors[] = new \Exception("Деление на ноль не возможно");
                        return false;
                    }
                    array_push($calcStack, $arg1 / $arg2);
                    break;
                default:
                    array_push($calcStack, $token);
            }
        }
        $this->result = array_pop($calcStack);
        return true;
    }

    /**
     * Получение списка ошибок вычислений массивом
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getResult(): string
    {
        return $this->result;
    }


}
