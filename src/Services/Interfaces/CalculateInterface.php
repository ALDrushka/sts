<?php

namespace App\Services\Interfaces;

interface CalculateInterface
{

    public function calculate(string $inputStr): bool;

    public function getErrors(): array;

    public function getResult(): string;
}