<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class DefaultControllerTest extends WebTestCase
{

    public function testIndex()
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCalculate()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/calculate',
            ['testInput' => '12 + 3']
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
