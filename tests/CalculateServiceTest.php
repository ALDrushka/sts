<?php
namespace App\Tests;

use App\Services\CalculateService;
use PHPUnit\Framework\TestCase;

class CalculateServiceTest extends TestCase
{
    public function testCalculate()
    {
        $calculator = new CalculateService();

        $testStr = "10+2* 3 + 6/2";
        $result = $calculator->calculate($testStr);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(true, $result);
        $this->assertEquals(19, $calculator->getResult());
    }

    public function testCalculateFalse()
    {
        $calculator = new CalculateService();

        $testStr = "10+2* 3 + 6/0";
        $result = $calculator->calculate($testStr);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(false, $result);
    }
}